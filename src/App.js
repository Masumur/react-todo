import logo from './logo.svg';
// import './App.css';
import './TodoApp.css';
import TodoList from "./components/TodoList";

function App() {
  return (
    <div className='todo-app'  style={{position: "relative"}}>
     <TodoList/>
        <h2 className="copyRight">Develop by Masum</h2>
    </div>
  );
}

export default App;
